package com.example.groomupapi.common.rest;

import com.example.groomupapi.GroomupapiApplication;
import com.example.groomupapi.common.application.dto.UserDTO;
import com.example.groomupapi.common.domain.User;
import com.example.groomupapi.common.domain.UserRepository;
import com.example.groomupapi.products.domain.FacebookProvider;
import com.example.groomupapi.products.domain.GMailProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = GroomupapiApplication.class)
@WebAppConfiguration
public class JwtAuthSteps {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    //    @Qualifier("_halObjectMapper")
    @Autowired
    ObjectMapper mapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

//    @Autowired
//    PlantInventoryItemRepository plantInventoryItemRepository;


//    private PurchaseOrderDTO purchaseOrder;

    private MvcResult requestResult;
    private Object data;
    private UserDTO responseUser;
//    private LocalDate newStartDate;
//    private LocalDate newEndDate;
//    private Long initialInventoryItemId;

    @Before  // Use `Before` from Cucumber library
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

//    @Autowired
//    private FilterChainProxy springSecurityFilterChain;

//    @Before
//    public void setup() {
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
//                .addFilter().build();
//    }

    @After  // Use `After` from Cucumber library
    public void tearOff() {
//        purchaseOrderRepository.deleteAll();
//        ttvRepository.deleteAll();
//        plantReservationRepository.deleteAll();
//        plantInventoryItemRepository.deleteAll();
//        plantInventoryEntryRepository.deleteAll();
    }

    @Given("^the following users$")
    public void the_following_services_categories(List<UserDTO> userDTOs) throws Throwable {
       List<User> users = new ArrayList<>();
        for(UserDTO  item  : userDTOs){
            item.setPassword( bcryptEncoder.encode(item.getPassword()));
            users.add(User.of(item.getId(), item.getUsername(), item.getPassword(), item.getRole()));
        }

        userRepository.saveAll(users);
    }

//    @Given("^no purchase order exists in the system$")
//    public void no_purchase_order_exists_in_the_system() throws Throwable {
//
////        GMailProvider newProvider = GMailProvider.of("117395254094023429418", "Odeyinka Olubunmi", "Olubunmi",
////                "Odeyinka", "photoUrl", "Google",  "token", "odeyinkao@gmail.com", "refreshToken", "accessToken");
//
//        //"/api/authenticate"
//        UserDTO user = UserDTO.of(0l,"baba123", "baba123", "");
//
//        requestResult = mockMvc.perform(MockMvcRequestBuilders
//                .post("/api/register")
//                .content(mapper.writeValueAsString(user))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        User token = mapper.readValue(
//                (requestResult.getResponse().getContentAsString()), User.class);
//
//        assertThat(token.getUsername()).isEqualTo(token.getUsername());
////                    {
////                    "name": "Odeyinka Olubunmi",
////                    "familyName": "Olubunmi",
////                    "givenName": "Odeyinka",
////                    "id": "117395254094023429418",
////                    "photoUrl": "https://lh5.googleusercontent.com/-JfJ21vnVNJg/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3reEg7w3qJgaXfvOqRDUCFgCjrICaQ/photo.jpg",
////                    "token": "ryryryueuy",
////                    "email": "odeyinkao@gmail.com",
////                    "refreshToken": "1//09ruH4gAnRFPoCgYIARAAGAkSNwF-L9IrI44tiYe8g4UlPdFNH4hvbYniAY_ErVA7cTLdjPpT6lVS3tMmiAZikAt5YLP8jv9FWsQ",
////                    "accessToken": "ya29.Il-vBxUETTctBaRJwyS03PouNDytROUplq1UOrv4A0uZaL8z2zIUA-JY-enoy7NxbFDPwnLL8fJvgpJ_glUEKwhcUZ7cfVHam8LANY08n3XykhI06CBp723lzCArveG-OQ",
////                    "sourceType": "Google"}
////        assertThat(purchaseOrderRepository.findAll().size()).isEqualTo(0);
//    }


//    @When("^tentative request with no success is made plant item with description \"([^\"]*)\" from \"([^\"]*)\" to \"([^\"]*)\"$")

//    @Given("^i passed token from logon to server i.e. {string}$", (String string))
//    public void no_purchase_order_exit(){
//        // Write code here that turns the phrase above into concrete actions
//        throw new cucumber.api.PendingException();
//    }

//    @Given("^i passed token from logon to server {string}")
//    public void i_passed_token_from_logon_to_server(String string){
//        // Write code here that turns the phrase above into concrete actions
//        throw new cucumber.api.PendingException();
//    }

////    @When("^i passed item to server i.e. {string}$")
//    @When("^i passed item to server$")
//    public void i_passed_item_to_serve(String string){
//        // Write code here that turns the phrase above into concrete actions
//        assertThat("token").isEqualTo("token");
////        throw new cucumber.api.PendingException();
//    }
//
//    @Then("^we should get response of Http (\\d+)$")
//    public void we_should_get_response_of_Http(int arg1) throws Throwable {
//        // Write code here that turns the phrase above into concrete actions
//        assertThat(requestResult.getResponse().getStatus()).isEqualTo(arg1);
//    }
//
//    @Then("^we should get response of that has field {string}")
//    public void we_should_get_response_of_that_has_field(String string){
//        // Write code here that turns the phrase above into concrete actions
//        throw new cucumber.api.PendingException();
//    }



    @When("^i try to login as existing user$")
    public void i_try_to_login_as_existing_user() throws Throwable {

        // Write code here that turns the phrase above into concrete actions
//        GMailProvider newProvider = GMailProvider.of("117395254094023429418", "Odeyinka Olubunmi", "Olubunmi",
//                "Odeyinka", "photoUrl", "Google",  "token", "odeyinkao@gmail.com", "refreshToken", "accessToken");
//
//        this.data = newProvider;
        UserDTO user = UserDTO.of(0l, "baba123", "baba123", "");
        this.data = user;

    }

    @When("^i get token and other data from gmail and passed to server$")
    public void iGetTokenAndOtherDataFromGmailAndPassedToServer() {
        GMailProvider newProvider = GMailProvider.of("117395254094023429418", "Odeyinka Olubunmi", "Olubunmi",
                "Odeyinka", "photoUrl", "Google",  "token", "odeyinkao@gmail.com", "refreshToken", "accessToken");

//        UserDTO user = UserDTO.of(0l, "baba123", "baba123", "");
        this.data = newProvider;
    }

    @When("^i passed data to server \"([^\"]*)\"$")
    public void i_passed_data_to_server(String arg1) throws Throwable {

        requestResult = mockMvc.perform(MockMvcRequestBuilders
                .post(arg1)
                .content(mapper.writeValueAsString(data))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Then("^i expect to get a user type of response$")
    public void i_expect_to_get_a_user_type_of_response() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        responseUser =  mapper.readValue(
                (requestResult.getResponse().getContentAsString()), UserDTO.class);

    }

    @Then("^i should get response of Http (\\d+)$")
    public void we_should_get_response_of_Http(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertThat(requestResult.getResponse().getStatus()).isEqualTo(arg1);
    }

    @Then("^i should get response of that has field \"([^\"]*)\"$")
    public void we_should_get_response_of_that_has_field(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Object token = mapper.readValue(
                (requestResult.getResponse().getContentAsString()), Object.class);

        assertThat(token.toString().contains("color"));
    }


    @Then("^i created an account with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_created_an_account_with_username(String arg1, String arg2) throws Throwable {

        this.data = UserDTO.of(0l,arg1, arg2, "CLIENT");

    }

    @Then("^i should get response where field \"([^\"]*)\" is \"([^\"]*)\"$")
    public void we_should_get_response_where_field(String arg1, String arg2) throws Throwable {

        switch (arg1) {
            case "username" :  assertThat(responseUser.getUsername()).isEqualTo(arg2);
        }

    }

    @When("^i get token and other data from facebbok and passed to server$")
    public void iGetTokenAndOtherDataFromFacebbokAndPassedToServer() {
        FacebookProvider newProvider = FacebookProvider.of("117395254094023429418", "Odeyinka Olubunmi", "photoUrl",
                "facebook", "token");
//        UserDTO user = UserDTO.of(0l, "baba123", "baba123", "");
        this.data = newProvider;
    }

}
