package com.example.groomupapi.services.rest;

import com.example.groomupapi.GroomupapiApplication;
import com.example.groomupapi.common.application.dto.UserDTO;
import com.example.groomupapi.common.domain.ServiceProvider;
import com.example.groomupapi.common.domain.User;
import com.example.groomupapi.common.domain.UserRepository;
import com.example.groomupapi.products.domain.GMailProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = GroomupapiApplication.class)
@WebAppConfiguration
public class ProductsSteps {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    private MockMvc mockMvc;

    private MvcResult requestResult;
    private Object data;
    private UserDTO responseUser;

    private String token;
//    private LocalDate newStartDate;
//    private LocalDate newEndDate;
//    private Long initialInventoryItemId;

    @Before  // Use `Before` from Cucumber library
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @After  // Use `After` from Cucumber library
    public void tearOff() {
//        purchaseOrderRepository.deleteAll();
//        ttvRepository.deleteAll();
//        plantReservationRepository.deleteAll();
//        plantInventoryItemRepository.deleteAll();
//        plantInventoryEntryRepository.deleteAll();
    }

    @Given("^the following users$")
    public void the_following_services_categories(List<UserDTO> userDTOs) throws Throwable {
        List<User> users = new ArrayList<>();
        for(UserDTO  item  : userDTOs){
            item.setPassword( bcryptEncoder.encode(item.getPassword()));
            users.add(User.of(item.getId(), item.getUsername(), item.getPassword(), item.getRole()));
        }

        userRepository.saveAll(users);
    }




    @When("^i try to login as existing user$")
    public void i_try_to_login_as_existing_user() throws Throwable {

        // Write code here that turns the phrase above into concrete actions
//        GMailProvider newProvider = GMailProvider.of("117395254094023429418", "Odeyinka Olubunmi", "Olubunmi",
//                "Odeyinka", "photoUrl", "Google",  "token", "odeyinkao@gmail.com", "refreshToken", "accessToken");
//
//        this.data = newProvider;
        UserDTO user = UserDTO.of(0l, "baba123", "baba123", "");
        this.data = user;

    }

    @When("^i get token and other data from gmail and passed to server$")
    public void iGetTokenAndOtherDataFromGmailAndPassedToServer() {
        GMailProvider newProvider = GMailProvider.of("117395254094023429418", "Odeyinka Olubunmi", "Olubunmi",
                "Odeyinka", "photoUrl", "Google",  "token", "odeyinkao@gmail.com", "refreshToken", "accessToken");

//        UserDTO user = UserDTO.of(0l, "baba123", "baba123", "");
        this.data = newProvider;
    }

    @When("^i passed data to server \"([^\"]*)\"$")
    public void i_passed_data_to_server(String arg1) throws Throwable {

        requestResult = mockMvc.perform(MockMvcRequestBuilders
                .post(arg1)
                .content(mapper.writeValueAsString(data))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Then("^i expect to get a user type of response$")
    public void i_expect_to_get_a_user_type_of_response() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        responseUser =  mapper.readValue(
                (requestResult.getResponse().getContentAsString()), UserDTO.class);

    }

    @Then("^i should get response of Http (\\d+)$")
    public void we_should_get_response_of_Http(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertThat(requestResult.getResponse().getStatus()).isEqualTo(arg1);
    }

    @Then("^i should get response of that has field \"([^\"]*)\"$")
    public void we_should_get_response_of_that_has_field(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Object value = mapper.readValue(
                (requestResult.getResponse().getContentAsString()), Object.class);

        assertThat(value.toString().contains(arg1));
    }


    @Then("^i created an account with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_created_an_account_with_username(String arg1, String arg2) throws Throwable {

        this.data = UserDTO.of(0l,arg1, arg2, "CLIENT");

    }

    @Then("^i should get response where field \"([^\"]*)\" is \"([^\"]*)\"$")
    public void we_should_get_response_where_field(String arg1, String arg2) throws Throwable {

        switch (arg1) {
            case "username" :  assertThat(responseUser.getUsername()).isEqualTo(arg2);
        }

    }

    @And("^the following categories in database$")
    public void theFollowingCategoriesInDatabase() {
    }

//    @When("^i created an provider account with title \"([^\"]*)\" and address \"([^\"]*)\"$")
//    public void iCreatedAnProviderAccountWithTitleAndAddress(String arg0, String arg1) throws Throwable {
//        // Write code here that turns the phrase above into concrete actions
//
//    }

    @When("^i passed data to server with token \"([^\"]*)\"$")
    public void iPassedDataToServerWithToken(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^i created an provider account with title \"([^\"]*)\" telephone \"([^\"]*)\" and address \"([^\"]*)\"$")
    public void iCreatedAnProviderAccountWithTitleTelephoneAndAddress(String arg0, String arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        ServiceProvider provider = ServiceProvider.of(null, null, arg0, arg1, "Blue", "url", arg2, 0,0, 0, false, false, false );
        data = provider;

//        Long id;
//
////    @OneToOne
////    User user;
//        @Column(name = "user_id")
//        String userId;
//
//        String title;
//
//        String phoneNo; // Todo: seperate to a list of contact model
//        String color;
//        String imageUrl;
//        String address;
//
//        //Todo: location like lat and long
//        int latitude;
//        int longitude;
//        int altitude;
//
//        Boolean hasAirConditioner;
//        Boolean allowPet;
//        Boolean allowChildren;
//        throw new PendingException();
    }

    @When("^i logged in as a client$")
    public void iLoggedInAsAClient() throws Throwable {

        Object userData = UserDTO.of(null, "baba123", "baba123", "");


        MvcResult loginResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/authenticate")
                .content(mapper.writeValueAsString(userData))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();


         HashMap<String, String> hash = mapper.readValue(
                (loginResult.getResponse().getContentAsString()), HashMap.class);

        token = hash.get("token");
    }

    @When("^i passed data to server with auth token \"([^\"]*)\"$")
    public void iPassedDataToServerWithAuthToken(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        requestResult = mockMvc
                .perform(MockMvcRequestBuilders
                .post(arg0)

                .header("Authorization", "Bearer " + token)
                .content(mapper.writeValueAsString(data))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

    }
}

//.contentType("application/json;charset=UTF-8")