package com.example.groomupapi.services.rest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty","html:target/cucumber"},
        features="classpath:features/services",
        glue="com.example.groomupapi.services.rest")
public class ProductsAcceptanceTestsRunner {
}
