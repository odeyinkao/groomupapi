Feature: Setting up Provider Account and Schedules
  As a logged on user
  So that I start setting my profile and schedules
  I want to have

  Background: Mu user account in and some categories in db
    Given the following users
      | id | username   | password        | role   |
      |  1 | baba123    | baba123         | CLIENT |
      |  2 | red0989    | red0989         | CLIENT |
#    And the following categories in database


  Scenario: Creating a provider account
    When i logged in as a client
    When i created an provider account with title "Bunny Saloon" telephone "636373737" and address "197 Jaama Tartu"
    When i passed data to server with auth token "/api/products/provider/details"
    Then i should get response of Http 200
    Then i should get response where field "title" is "Bunny Saloon"
    Then i should get response where field "address" is "197 Jaama Tartu"

#  Scenario: Logging in with Username and Password
#    When i try to login as existing user
#    When i passed data to server "/api/authenticate"
#    Then i should get response of Http 200
#    Then i should get response of that has field "token"
#
#
#  Scenario: Logging in with Gmail
#    When i get token and other data from gmail and passed to server
#    When i passed data to server "/api/authenticate/gmail"
#    Then i should get response of Http 200
#    Then i should get response of that has field "token"


