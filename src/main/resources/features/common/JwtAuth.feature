Feature: Authenticate in GroomUp API
  As a Groom-up generic User

  Background: With some sample users
    Given the following users
      | id | username   | password        | role   |
      |  1 | baba123    | baba123         | CLIENT |
      |  2 | red0989    | red0989         | CLIENT |


  Scenario: Creating user account from username and password
    When i created an account with username "Bunny" and password "rabbit"
    When i passed data to server "/api/register"
    Then i expect to get a user type of response
    Then i should get response of Http 200
    Then i should get response where field "username" is "Bunny"

  Scenario: Logging in with Username and Password
    When i try to login as existing user
    When i passed data to server "/api/authenticate"
    Then i should get response of Http 200
    Then i should get response of that has field "token"


  Scenario: Logging in with Gmail
    When i get token and other data from gmail and passed to server
    When i passed data to server "/api/authenticate/gmail"
    Then i should get response of Http 200
    Then i should get response of that has field "token"

  Scenario: Logging in with Facebook
    When i get token and other data from facebbok and passed to server
    When i passed data to server "/api/authenticate/facebook"
    Then i should get response of Http 200
    Then i should get response of that has field "token"