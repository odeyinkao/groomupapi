insert into category (id, color, title) values (1, '#9eecff', 'HAIR SALOON');
insert into category (id, color, title) values (2, '#b9ffb0', 'FACE TREATMENTS');
insert into category (id, color, title) values (3, '#ffc7ff', 'NAILS');
insert into category (id, color, title) values (4, '#47fced', 'HAIR REMOVAL');
insert into category (id, color, title) values (5, '#368dff', 'MAKEUP/LASHES/BROWNS');
insert into category (id, color, title) values (6, '#41d95d', 'MED SPA');


-- insert into user (id, username, password) values (1, 'Peggy', 'peggy')
-- insert into service_provider ( id, title, address, allow_children, allow_pet, has_air_conditioner, image_url, phone_no, user_id, latitude, longitude, altitude) values (1, 'Peggy Salon', 'Jaama 97, Tartu', true, false, true, '01jpg', '040848484', 1, 0,0,0);
-- update user set service_provider_id = 1 where id = 1
-- insert into category_service_provider (id, service_id, category_id) values (1, 1, 1);
-- insert into category_service_provider (id, service_id, category_id) values (2, 1, 2);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (1, '2019-08-23 12:00', '2019-08-23 12:45', 'OPEN', 1, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (2, '2019-08-23 13:00', '2019-08-23 13:30', 'OPEN', 1, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (3, '2019-08-23 15:00', '2019-08-23 15:40', 'OPEN', 1, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (4, '2019-08-23 15:55', '2019-08-23 16:40', 'OPEN', 2, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (5, '2019-08-23 17:30', '2019-08-23 18:25', 'OPEN', 2, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (6, '2019-08-23 18:50', '2019-08-23 19:35', 'OPEN', 2, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (7, '2019-08-23 12:00', '2019-08-23 12:45', 'OPEN', 2, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (8, '2019-08-23 15:00', '2019-08-23 15:40', 'OPEN', 2, 1);
-- insert into schedule (id, start, end, status, category_id, service_provider_id) values (9, '2019-08-23 18:50', '2019-08-23 19:35', 'OPEN', 1, 1);

--     Long id;
--     BusinessPeriod period;
--     @Enumerated(EnumType.STRING)
--     ScheduleStatus status;
--     affordability,
--
--     imageUrl,
--     duration,
--     schedules,
--
--
-- new Service(
--     "s1",
--     ["c1", "c2"],
--     "Peggy Salon",
--     "affordable",
--     "simple",
--     "01jpg",
--     20,
--     // id, date, start, end, categoryId
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s11", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s12", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2")
--     ],
--     false,
--     true,
--     true
--   ),
--
--   new Service(
--     "s2",
--     ["c5", "c6"],
--     "Resort Platinum Salon",
--     "affordable",
--     "simple",
--     "02jpg",
--     10,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1")
--     ],
--     false,
--     false,
--     false
--   ),
--
--   new Service(
--     "s3",
--     ["c1", "c3"],
--     "Classic Style Salon",
--     "pricey",
--     "simple",
--     "03jpg",
--     45,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s11", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2")
--     ],
--     false,
--     false,
--     false
--   ),
--
--   new Service(
--     "s4",
--     ["c3", "c5"],
--     "Refined Style Salon",
--     "luxurious",
--     "challenging",
--     "04jpg",
--     60,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2")
--     ],
--     false,
--     false,
--     false
--   ),
--
--   new Service(
--     "s5",
--     ["c2", "c4", "c6"],
--     "K2k Salon",
--     "luxurious",
--     "simple",
--     "05jpg",
--     45,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1")
--     ],
--     true,
--     false,
--     true
--   ),
--
--   new Service(
--     "s6",
--     ["c1", "c2"],
--     "Smooth Style Salon",
--     "affordable",
--     "hard",
--     "06jpg",
--     120,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s11", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s12", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2")
--     ],
--     true,
--     false,
--     true
--   ),
--
--   new Service(
--     "s7",
--     ["c4", "c5"],
--     "Divine Style Salon",
--     "affordable",
--     "simple",
--     "07jpg",
--     30,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2")
--     ],
--     true,
--     false,
--     true
--   ),
--
--   new Service(
--     "s8",
--     ["c2"],
--     "Rock & Roll Salon",
--     "pricey",
--     "challenging",
--     "08jpg",
--     35,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1")
--     ],
--     true,
--     false,
--     false
--   ),
--
--   new Service(
--     "s9",
--     ["c4"],
--     "Creepy Dude Salon",
--     "affordable",
--     "hard",
--     "09jpg",
--     45,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s11", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2")
--     ],
--     true,
--     false,
--     true
--   ),
--   new Service(
--     "s10",
--     ["c2", "c5", "c3"],
--     "Super Hero Salon",
--     "luxurious",
--     "simple",
--     "10jpg",
--     30,
--     [
--       new Schedule("s1", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s2", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s3", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s4", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s5", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s6", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2"),
--       new Schedule("s7", "23-Oct-2019", "12:00 PM", "12:45 PM", "c1"),
--       new Schedule("s8", "23-Oct-2019", "13:00 PM", "13:30 PM", "c1"),
--       new Schedule("s9", "23-Oct-2019", "15:00 PM", "15:40 PM", "c1"),
--       new Schedule("s10", "23-Oct-2019", "15:55 PM", "16:40 PM", "c2"),
--       new Schedule("s11", "23-Oct-2019", "17:30 PM", "18:25 PM", "c2"),
--       new Schedule("s12", "23-Oct-2019", "18:50 PM", "19:35 PM", "c2")
--     ],
--     true,
--     true,
--     true
--   )