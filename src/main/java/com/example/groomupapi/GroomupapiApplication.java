package com.example.groomupapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroomupapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GroomupapiApplication.class, args);
    }

}
