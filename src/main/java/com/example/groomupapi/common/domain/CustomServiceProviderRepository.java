package com.example.groomupapi.common.domain;

import java.util.List;

public interface CustomServiceProviderRepository {
    List<ServiceProvider> findProviderByServiceCategory(Long categoryId);
//    List<ServiceProvider> confirmItemAvailableAtPeriod(Long id, LocalDate start, LocalDate end);
}