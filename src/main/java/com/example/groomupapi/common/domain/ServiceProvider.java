package com.example.groomupapi.common.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class ServiceProvider {

    @Id
    @GeneratedValue
    Long id;

//    @OneToOne
//    User user;
    @Column(name = "user_id")
    String userId;

    String title;

    String phoneNo; // Todo: seperate to a list of contact model
    String color;
    String imageUrl;
    String address;

    //Todo: location like lat and long
    int latitude;
    int longitude;
    int altitude;

    Boolean hasAirConditioner;
    Boolean allowPet;
    Boolean allowChildren;

    public void setId(Long id){
        this.id = id;
    }

//    @OneToMany
//    List<Schedule> schedules;

//    @ManyToMany
//    List<Category> categories;


//    public static ServiceProvider of(Long id, String title, String color, String imageUrl, String phoneNo, int latitude,
//            int longitude,
//            int altitude,
//            Boolean hasAirConditioner, Boolean allowPet, Boolean allowChildren) {
//        ServiceProvider srv = new ServiceProvider();
//        srv.id = id;
//        srv.title = title;
//        srv.color = color;
//        srv.imageUrl = imageUrl;
//        srv.phoneNo = phoneNo;
//
//        srv.latitude = latitude;
//        srv.longitude = longitude;
//        srv.altitude = altitude;
//
//        srv.hasAirConditioner = hasAirConditioner;
//        srv.allowPet = allowPet;
//        srv.allowChildren = allowChildren;
//
//        return srv;
//    }
}