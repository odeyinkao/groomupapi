package com.example.groomupapi.common.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServiceProviderRepository extends JpaRepository<ServiceProvider, Long>, CustomServiceProviderRepository {
//    @Query("select srv from ServiceProvider srv where srv in (select cat.ServiceProviderId from ServiceProviderCategories cat where cat.CategoriesId = ?1)")
//    List<ServiceProvider> findProviderByServiceCategory(Long category);
    @Query("select srv from ServiceProvider srv where srv.userId = ?1")
    Optional<ServiceProvider> findProviderByUserId(String provider);

}
