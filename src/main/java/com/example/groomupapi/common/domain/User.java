package com.example.groomupapi.common.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class User {
    @Id
    @GeneratedValue
    Long id;
    String username;

    @JsonIgnore
    String password;

    String role;

    @OneToOne
    Customer customer;

    @OneToOne
    ServiceProvider serviceProvider;

    public static User of(String username, String password, String role){
        User user = new User();
        user.username = username;
        user.password = password;
        user.role = role;

        return  user;
    }

    public static User of(Long id, String username, String password, String role){
        User user = User.of(username, password, role);
        user.id =id;
        return  user;
    }

    public void setPassword(String password){
        this.password = password;
    }
}
