package com.example.groomupapi.common.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select p from User p where LOWER(p.username) = ?1")
    Optional<User> findByUsername(String username);

}