package com.example.groomupapi.common.domain;

import com.example.groomupapi.products.domain.Schedule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class Customer {

    @Id
    @GeneratedValue
    Long id;

//    @OneToOne
//    User user;

    @Column(name = "user_id")
    Long userId;

    String avatarUrl;

    @OneToMany
    List<ServiceProvider> favorite;

    @OneToMany
    List<Schedule> schedules;


    public static Customer of(Long userId, String avatarUrl) {
        Customer ctg = new Customer();
        ctg.userId = userId;
        ctg.avatarUrl = avatarUrl;

        return ctg;
    }
}
