package com.example.groomupapi.common.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class ServiceProviderRepositoryImpl implements CustomServiceProviderRepository {
    @Autowired
    EntityManager em;

    public List<ServiceProvider> findProviderByServiceCategory(Long categoryId) {
        return em.createQuery("select s from ServiceProvider s where s.id in " +
                                "(select cat.serviceProviderId from CategoryServiceProvider cat where cat.categoryId = ?1)",
                ServiceProvider.class)
                .setParameter(1, categoryId)
                .getResultList();
    }
}