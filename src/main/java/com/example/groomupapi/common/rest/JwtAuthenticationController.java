package com.example.groomupapi.common.rest;

import com.example.groomupapi.common.application.dto.UserDTO;
import com.example.groomupapi.common.application.service.JwtUserDetailsService;
import com.example.groomupapi.common.domain.JwtRequest;
import com.example.groomupapi.common.domain.JwtResponse;
import com.example.groomupapi.common.domain.User;
import com.example.groomupapi.common.domain.UserRepository;
import com.example.groomupapi.config.JwtTokenUtil;
import com.example.groomupapi.products.domain.FacebookProvider;
import com.example.groomupapi.products.domain.FacebookProviderRepository;
import com.example.groomupapi.products.domain.GMailProvider;
import com.example.groomupapi.products.domain.GmailAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private GmailAccountRepository gmailAccountRepository;
    @Autowired
    private FacebookProviderRepository facebookProviderRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
        return ResponseEntity.ok(userDetailsService.save(user));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @PostMapping("/authenticate/facebook")
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<?>  loginFacebook(@RequestBody FacebookProvider facebook, HttpServletRequest request) throws Exception {
        //Todo: need to confirm token from facebook
        try {

        //Todo: need to confirm token from gmail
        FacebookProvider facebookProvider = facebookProviderRepository.findById(facebook.getId()).orElse(null);
        User user = userRepository.findByUsername(facebook.getId()+"_"+facebook.getSourceType().toLowerCase()).orElse(null);

        String baseUrlString = getBaseUrl(request);


        facebook.setToken(null);

        if(facebookProvider == null){
            facebookProviderRepository.save(facebook);
        }

        if(user == null){
            user = userDetailsService.save(UserDTO.of(null, facebook.getId()+"_"+facebook.getSourceType().toLowerCase(), facebook.getId()+"_"+facebook.getSourceType().toLowerCase(),"CLIENT" ));
        }


//        user.setPassword(facebook.getId()+"_"+facebook.getSourceType().toLowerCase());

//        authenticate(user.getUsername(), user.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(user.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(e.getMessage());
    }

    }

    private String getBaseUrl(HttpServletRequest request) throws MalformedURLException {
        URL requestURL = new URL(request.getRequestURL().toString());
        String port = requestURL.getPort() == -1 ? "" : ":" + requestURL.getPort();
        return requestURL.getProtocol() + "://" + requestURL.getHost() + port;
    }

    @PostMapping("/authenticate/gmail")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> loginGmail(@RequestBody GMailProvider gmail, HttpServletRequest request) throws Exception {
        //Todo: need to confirm token from gmail

        try {
            GMailProvider gmailProvider = gmailAccountRepository.findById(gmail.getId()).orElse(null);
            User user = userRepository.findByUsername(gmail.getId() + "_" + gmail.getSourceType().toLowerCase()).orElse(null);

            String baseUrlString = getBaseUrl(request);


            gmail.setToken(null);

            if (gmailProvider == null) {
                gmailAccountRepository.save(gmail);
            }

            if (user == null) {
                user = userDetailsService.save(UserDTO.of(null, gmail.getId() + "_" + gmail.getSourceType().toLowerCase(), gmail.getId() + "_" + gmail.getSourceType().toLowerCase(), "CLIENT"));
            }


//        user.setPassword(gmail.getId()+"_"+gmail.getSourceType().toLowerCase());

            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(user.getUsername());
            final String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtResponse(token));
        }catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(e.getMessage());
        }

    }


}