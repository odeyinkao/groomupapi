package com.example.groomupapi.common.application.service;

import com.example.groomupapi.common.application.dto.UserDTO;
import com.example.groomupapi.common.domain.User;
import com.example.groomupapi.common.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		List<User> users = userRepository.findAll();
		User user = userRepository.findByUsername(username).orElse(null);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}

		List<GrantedAuthority> role = new ArrayList<>();
		role.add(new GrantedAuthority() {
			@Override
			public String getAuthority() {
				return user.getRole();
			}
		});

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				role);
	}

	public User save(UserDTO user) {
		String password = bcryptEncoder.encode(user.getPassword());
		User newUser = User.of(user.getUsername(), password, user.getRole());
		return userRepository.save(newUser);
	}
}