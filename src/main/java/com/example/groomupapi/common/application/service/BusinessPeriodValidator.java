package com.example.groomupapi.common.application.service;

import com.example.groomupapi.common.domain.BusinessPeriod;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDateTime;

@Service
public class BusinessPeriodValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return BusinessPeriod.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BusinessPeriod bp = (BusinessPeriod) o;

        if(bp.getStart() == null)
            errors.rejectValue("start", "start cannot be null");
        if(bp.getEnd() == null)
            errors.rejectValue("end", "end cannot be null");
        if(bp.getStart() != null && bp.getEnd() != null) {
            if (!bp.getStart().isBefore(bp.getEnd()))
                errors.rejectValue("start", "start must ocurrs before end");
            if (bp.getStart().isBefore(LocalDateTime.now()))
                errors.rejectValue("start", "start must be in the future");
            if (bp.getEnd().isBefore(LocalDateTime.now()))
                errors.rejectValue("end", "end must be in the future");
        }
    }
}
