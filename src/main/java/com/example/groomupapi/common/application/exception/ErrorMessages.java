package com.example.groomupapi.common.application.exception;

public interface ErrorMessages {

    String Invalid_PO_Period = "Invalid PO Period";
    String No_Available_Item = "No available items";
    String Invalid_Input_Item = "Invalid Plant Item";
    String Invalid_PO = "Invalid PO";
    String Plant_NOT_Found =  "Plant NOT Found";
    String PO_NOT_Exiting = "PO NOT Exiting";
    String PO_Not_Accepted = "PO Not Accepted";
    String StartDate_In_Future =  "startDate must be in the future";

}