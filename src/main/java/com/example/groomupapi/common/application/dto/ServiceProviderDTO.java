package com.example.groomupapi.common.application.dto;

import com.example.groomupapi.products.application.dto.ScheduleDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class ServiceProviderDTO {

    @Id
    @GeneratedValue
    Long id;

//    @OneToOne
    String userId;

    String phoneNo; // Todo: seperate to a list of contact model
    String color;
    String imageUrl;
    String address;
    String title;

    //Todo: location like lat and long
    int latitude;
    int longitude;
    int altitude;

    Boolean hasAirConditioner;
    Boolean allowPet;
    Boolean allowChildren;

    @OneToMany
    List<ScheduleDTO> schedules;

    public void setSchedules(List<ScheduleDTO> schedules){
        this.schedules = schedules;
    }
    public void setId(Long id){this.id = id;}
//
//    @ManyToMany
//    List<Category> categories;


//    public static ServiceProviderDTO of(Long id, User user, String color, String imageUrl, String phoneNo, String address, String title, Boolean hasAirConditioner, Boolean allowPet, Boolean allowChildren) {
//        ServiceProviderDTO srv = new ServiceProviderDTO();
//        srv.id = id;
////        srv.user = user;
//
//        srv.phoneNo = phoneNo;
//        srv.color = color;
//        srv.imageUrl = imageUrl;
//
//        srv.address = address;
//        srv.title = title;
//
//        srv.hasAirConditioner = hasAirConditioner;
//        srv.allowPet = allowPet;
//        srv.allowChildren = allowChildren;
//
//        return srv;
//    }
}
