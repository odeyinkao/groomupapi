package com.example.groomupapi.common.application.dto;

import com.example.groomupapi.products.application.dto.ScheduleDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CustomerDTO {

    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    UserDTO user;
//    @Column(title="user_id")
//    Long userId;

    String avatarUrl;

    @OneToMany
    List<ServiceProviderDTO> favorite;

    @OneToMany
    List<ScheduleDTO> schedules;

}
