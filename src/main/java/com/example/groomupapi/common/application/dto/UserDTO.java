package com.example.groomupapi.common.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class UserDTO {
    @Id
    Long id;
    String username;
    String password;
    String role;

//    @OneToOne
//    CustomerDTO customer;
//
//    @OneToOne
//    ServiceProviderDTO serviceProvider;

//    public static UserDTO of(Long id, String username, String password, String role){
//        UserDTO user = new UserDTO();
//        user.id = id;
//        user.username = username;
//        user.password = password;
//        user.role = role;
//
//        return  user;
//    }
}
