package com.example.groomupapi.products.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CategoryDTO {
    Long id;
    String title;
    String color;
    String url;

//    public static CategoryDTO of(Long id, String title, String color, String url) {
//        CategoryDTO ctg = new CategoryDTO();
//        ctg.id = id;
//        ctg.title = title;
//        ctg.color = color;
//        ctg.url = url;
//
//        return ctg;
//    }

}
