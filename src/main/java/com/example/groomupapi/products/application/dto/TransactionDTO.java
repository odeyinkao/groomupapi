package com.example.groomupapi.products.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class TransactionDTO {
    Long id;
    Long scheduleId;
    double amount;
    LocalDateTime date;
    String comment;

}
