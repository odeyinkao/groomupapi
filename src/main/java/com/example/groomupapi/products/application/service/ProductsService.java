package com.example.groomupapi.products.application.service;

import com.example.groomupapi.common.application.dto.BusinessPeriodDTO;
import com.example.groomupapi.common.application.dto.ServiceProviderDTO;
import com.example.groomupapi.common.domain.*;
import com.example.groomupapi.products.application.dto.CategoryDTO;
import com.example.groomupapi.products.application.dto.ScheduleDTO;
import com.example.groomupapi.products.application.dto.ScheduleID;
import com.example.groomupapi.products.application.dto.TransactionDTO;
import com.example.groomupapi.products.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsService {
    @Autowired
    CategoryRepository categoryService;
    @Autowired
    ServiceProviderRepository serviceProviderRepository;
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryServiceRepository categoryServiceRepository;
    @Autowired
    FavoriteProviderRepository favoriteProviderRepository;


    public List<CategoryDTO> getCategories() {
        return categoryService.findAll().stream().map(item -> CategoryDTO.of(item.getId(), item.getTitle(), item.getColor(), item.getUrl())).collect(Collectors.toList());
    }

    public List<ServiceProviderDTO> getProviders(Long id) {
        return serviceProviderRepository.findProviderByServiceCategory(id).stream().map(item -> toServiceProviderDTO(item)).collect(Collectors.toList());
    }

    public List<ScheduleDTO> getSchedules(Long ownerId, Long categoryId) {
        ServiceProvider provider = serviceProviderRepository.findById(ownerId).orElse(null);
        return scheduleRepository.findScheduleByProviderAndCategory(provider.getUserId(), categoryId).stream().map(item -> ScheduleDTO.of(item.getId(), BusinessPeriodDTO.of(item.getPeriod().getStart(), item.getPeriod().getEnd()), item.getStatus(), item.getServiceProviderId(), item.getCategoryId(), item.getAllocatedId(), null)).collect(Collectors.toList());
    }

    public List<ServiceProviderDTO> getFavoriteProviders(String userId) {
//       return favoriteProviderRepository.findFavoriteByUserId(userId).stream().map(item ->  toServiceProviderDTO(item)).collect(Collectors.toList());
        //This a very poor implementation
         List<ServiceProviderDTO> favoriteServices = new ArrayList<>();
         List<FavoriteProvider> favoriteProviders = favoriteProviderRepository.findFavoriteByUserId(userId).stream().collect(Collectors.toList());
         for(FavoriteProvider favoriteProvider: favoriteProviders){
             ServiceProvider provider =  serviceProviderRepository.findById(favoriteProvider.getProvider_id()).orElse(null);
             if (provider != null) {
                 favoriteServices.add(toServiceProviderDTO(provider));
             }
         }

        return favoriteServices;
    }

    public ServiceProviderDTO createFavoriteProviders(FavoriteProvider favoriteProvider){

//        favoriteProvider.setId(null);
        favoriteProviderRepository.save(favoriteProvider);
        ServiceProvider  serviceProvider = serviceProviderRepository.findById(favoriteProvider.getProvider_id()).orElse(null);

        if(serviceProvider != null) {
            return toServiceProviderDTO(serviceProvider);
        } else {return null;}
    }

    public void deleteFavoriteProviders(String userId, Long id){

        List<FavoriteProvider> fav =  favoriteProviderRepository.findAll();

        FavoriteProvider favprod = favoriteProviderRepository.findFavoriteByUserAndProviderId(userId, id).orElse(null);
        if(favprod != null) {
            favoriteProviderRepository.deleteById(favprod.getId());
        }
    }

//    public UserDTO getUser(Long id) {
//        User user = userRepository.findById(id).orElse(null);
//
//        UserDTO userDTO = UserDTO.of(user.getId(), user.getUsername(), "",  user.getRole(), null, null);
//
//        if(user.getCustomer() != null){
//            List<ServiceProviderDTO> setProviderDTOS = user.getCustomer().getFavorite().stream().map(item ->  toServiceProviderDTO(item)).collect(Collectors.toList());
//            userDTO.setCustomer(CustomerDTO.of(user.getCustomer().getId(), null, user.getCustomer().getAvatarUrl(), setProviderDTOS, null));
//        }
//
//
//        if(user.getServiceProvider() != null){
//            userDTO.setServiceProvider(toServiceProviderDTO(user.getServiceProvider()));
//        }
//        return userDTO;
//    }

    public ServiceProviderDTO getProviderAndServices(String userId) {
        //Todo: Use authentication to filter
        ServiceProvider provider =  serviceProviderRepository.findProviderByUserId(userId).orElse(null);

        if(provider != null){

            List<ScheduleDTO> scheduleList =  scheduleRepository.findScheduleByOwner(userId).stream().map(item -> ScheduleDTO.of(item.getId(), BusinessPeriodDTO.of(item.getPeriod().getStart(), item.getPeriod().getEnd()), item.getStatus(), item.getServiceProviderId(), item.getCategoryId(), item.getAllocatedId(), null)).collect(Collectors.toList());

            ServiceProviderDTO serviceProviderDTO =  toServiceProviderDTO(provider);

            serviceProviderDTO.setSchedules(scheduleList);
            return serviceProviderDTO;
        }


        return null;
    }


   public ScheduleDTO allocateSchedule(ScheduleID scheduleID, String alloded_to){
      Schedule schedule = scheduleRepository.findById(scheduleID.getId()).orElse(null);

      if(schedule.getStatus() != ScheduleStatus.BOOKED) {
          schedule.setAllocatedId(alloded_to);
          schedule.setStatus(ScheduleStatus.BOOKED);

          scheduleRepository.save(schedule);
      }

      return  ScheduleDTO.of(schedule.getId(), BusinessPeriodDTO.of(schedule.getPeriod().getStart(), schedule.getPeriod().getEnd()), schedule.getStatus(), schedule.getServiceProviderId() ,schedule.getCategoryId(), schedule.getAllocatedId(), null);
   }

    public ServiceProviderDTO updateProviderDetails(ServiceProviderDTO provider) {
        //Todod query db and get the present state f item before update so that u dont get null
        // in field with empty values or
        ServiceProvider serviceProvider = toServiceProvider(provider);
        serviceProviderRepository.save(serviceProvider);
        return toServiceProviderDTO( serviceProvider);

    }

    public ServiceProviderDTO createProviderDetails(ServiceProviderDTO provider) {
        ServiceProvider serviceProvider = toServiceProvider(provider);
        serviceProvider.setId(null);
        serviceProviderRepository.save(serviceProvider);
        return toServiceProviderDTO( serviceProvider);

    }

    public List<ScheduleDTO> fetchProviderBookings(String providerAccount) {
        List<Schedule> schedules = scheduleRepository.fetchProviderBookings(providerAccount).stream().collect(Collectors.toList());
        return toScheduleDTOS(schedules);
    }

    public List<ScheduleDTO> fetchClientBookings(String clientAccount) {
        List<Schedule> schedules =  scheduleRepository.fetchClientBookings(clientAccount).stream().collect(Collectors.toList());

        return toScheduleDTOS(schedules);

    }

    public ScheduleDTO createServiceSchedule(ScheduleDTO schedule, String userId) {
        Schedule newSchedule =
                Schedule.of(BusinessPeriod.of(schedule.getPeriod().getStart(), schedule.getPeriod().getEnd()), schedule.getStatus(), schedule.getCategoryId(), userId);

        scheduleRepository.save(newSchedule);
        updateProviderCategoryServices(newSchedule.getCategoryId(), userId);

        return toScheduleDTO(newSchedule);
    }

    public ScheduleDTO updateServiceSchedule(ScheduleDTO schedule, String userId) {
        Schedule updateSchedule = scheduleRepository.findById(schedule.getId()).orElse(null);
        updateSchedule.setStatus(schedule.getStatus());
        updateSchedule.setPeriod(BusinessPeriod.of(schedule.getPeriod().getStart(), schedule.getPeriod().getEnd()));
//        updateSchedule.setStatus(schedule.getStatus());
        updateSchedule.setCategoryId(schedule.getCategoryId());

        scheduleRepository.save(updateSchedule);

        updateProviderCategoryServices(updateSchedule.getCategoryId(), userId);
        return toScheduleDTO(updateSchedule);
    }

    public List<ScheduleDTO> toScheduleDTOS(List<Schedule> schedules){
        List<ScheduleDTO> scheduleDTOS = new ArrayList<ScheduleDTO>();
        for (Schedule item: schedules) {
            ScheduleDTO scheduleDTO = toScheduleDTO(item);
            scheduleDTOS.add(scheduleDTO);
        }
        return scheduleDTOS;
    }
    public ScheduleDTO toScheduleDTO(Schedule schedule){
        ScheduleDTO scheduleDTO = ScheduleDTO.of(schedule.getId(), BusinessPeriodDTO.of(schedule.getPeriod().getStart(), schedule.getPeriod().getEnd()), schedule.getStatus() , schedule.getServiceProviderId(), schedule.getCategoryId(),schedule.getAllocatedId(), null);

        if(schedule.getTransaction() != null){
            TransactionDTO transactionDTO = TransactionDTO.of(schedule.getTransaction().getId(), schedule.getTransaction().getScheduleId(), schedule.getTransaction().getAmount(), schedule.getTransaction().getDate(), schedule.getTransaction().getComment());
            scheduleDTO.setTransaction(transactionDTO);
        }
        return  scheduleDTO;
    }

    public ServiceProviderDTO toServiceProviderDTO(ServiceProvider serviceProvider){

       ServiceProviderDTO serviceProviderDTO =  ServiceProviderDTO.of(serviceProvider.getId(), serviceProvider.getUserId(), serviceProvider.getPhoneNo(), serviceProvider.getColor(), serviceProvider.getImageUrl(), serviceProvider.getAddress(), serviceProvider.getTitle(), serviceProvider.getLatitude(),
        serviceProvider.getLongitude(), serviceProvider.getAltitude(), serviceProvider.getHasAirConditioner(), serviceProvider.getAllowPet(), serviceProvider.getAllowChildren(), null);

       return serviceProviderDTO;
    }

    public ServiceProvider toServiceProvider(ServiceProviderDTO serviceProviderDTO){

        ServiceProvider serviceProvider =  ServiceProvider.of(serviceProviderDTO.getId(), serviceProviderDTO.getUserId(), serviceProviderDTO.getTitle(), serviceProviderDTO.getPhoneNo(), serviceProviderDTO.getColor(), serviceProviderDTO.getImageUrl(), serviceProviderDTO.getAddress(), serviceProviderDTO.getLatitude(),
                serviceProviderDTO.getLongitude(), serviceProviderDTO.getAltitude(), serviceProviderDTO.getHasAirConditioner(), serviceProviderDTO.getAllowPet(), serviceProviderDTO.getAllowChildren());

        return serviceProvider;
    }

    private void updateProviderCategoryServices(Long category, String userId){
        ServiceProvider provider = serviceProviderRepository.findProviderByUserId(userId).orElse(null);

        //Todo remove the modified or deleted out category from the categoryServiceProvider list
        CategoryServiceProvider csp = categoryServiceRepository.findCSPByServiceAndCategory(category, provider.getId()).orElse(null);
        if(csp == null){
            categoryServiceRepository.save(CategoryServiceProvider.of(category, provider.getId()));
        }
    }
}