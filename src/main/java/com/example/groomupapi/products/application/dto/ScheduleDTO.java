package com.example.groomupapi.products.application.dto;

import com.example.groomupapi.common.application.dto.BusinessPeriodDTO;
import com.example.groomupapi.products.domain.ScheduleStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@JsonIgnoreProperties
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class ScheduleDTO {
    Long id;

    BusinessPeriodDTO period;

    @Enumerated(EnumType.STRING)
    ScheduleStatus status;

//    Long ownerId;

    String serviceProviderId;

    Long categoryId;

    String allocatedId;

    TransactionDTO transaction;


//    @Builder
//    @SuppressWarnings("unused")
//    public ScheduleDTO(Long id, BusinessPeriodDTO period, ScheduleStatus status, Long serviceProviderId,  Long categoryId, Long allocationId, TransactionDTO transaction){
//        this.id = id;
//        this.period = period;
//        this.status = status;
//        this.serviceProviderId = serviceProviderId;
//
//        this.categoryId = categoryId;
//
//        this.allocationId = allocationId;
//        this.transaction = Optional.ofNullable(transaction).orElse(this.transaction);
//    }
}