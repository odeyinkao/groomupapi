package com.example.groomupapi.products.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class ScheduleID {
    Long id;

    Long providerId;

}