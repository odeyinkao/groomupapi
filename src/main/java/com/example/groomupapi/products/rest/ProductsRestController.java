package com.example.groomupapi.products.rest;

import com.example.groomupapi.common.application.dto.ServiceProviderDTO;
import com.example.groomupapi.common.application.exception.ErrorMessages;
import com.example.groomupapi.products.application.dto.ScheduleDTO;
import com.example.groomupapi.products.application.dto.ScheduleID;
import com.example.groomupapi.products.application.service.ProductsService;
import com.example.groomupapi.products.domain.FavoriteProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/products")
public class ProductsRestController {


    @Autowired
    ProductsService productService;

    @GetMapping("/categories")
    public ResponseEntity<?> fetchCategories() throws Exception {

        try {
            return new ResponseEntity<>(productService.getCategories(), HttpStatus.OK);
        }catch (Exception err){

            switch(err.getMessage()) {
                case ErrorMessages.Invalid_Input_Item:
                case ErrorMessages.Invalid_PO_Period :
                case ErrorMessages.No_Available_Item :
                    return new ResponseEntity<>( HttpStatus.CONFLICT);
                case ErrorMessages.Invalid_PO :
                    return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
                default : // Optional

                    return new ResponseEntity<>(err ,HttpStatus.BAD_REQUEST);
            }
        }
    }

    @GetMapping("/providers/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ServiceProviderDTO> fetchProviders(@PathVariable Long id) throws Exception {
        return  productService.getProviders(id);
    }

    @GetMapping("/schedules/{ownerId}/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleDTO> fetchSchedules(@PathVariable Long ownerId, @PathVariable Long categoryId) throws Exception {
        return productService.getSchedules(ownerId, categoryId);
    }

    @GetMapping("/client/favorites")
    @ResponseStatus(HttpStatus.OK)
    public List<ServiceProviderDTO> fetchFavoriteProviders() throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return productService.getFavoriteProviders(authentication.getName());
    }

    @GetMapping("/client/favorites/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ServiceProviderDTO createFavoriteProviders(@PathVariable Long id) throws Exception {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        FavoriteProvider favoriteProvider = FavoriteProvider.of(null, authentication.getName(), id);
        return productService.createFavoriteProviders(favoriteProvider);
    }

    @DeleteMapping("/client/favorites/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteFavoriteProviders(@PathVariable Long id) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        productService.deleteFavoriteProviders(authentication.getName(), id);
    }

//    @GetMapping("/users/{id}")
//    @ResponseStatus(HttpStatus.OK)
//    public UserDTO fetchUser(@PathVariable Long id) throws Exception {
//        return productService.getUser(id);
//    }

    @GetMapping("/provider/services")
    @ResponseStatus(HttpStatus.OK)
    public ServiceProviderDTO fetchProviderAndServices() throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return productService.getProviderAndServices(authentication.getName());
    }

    @GetMapping("/provider/bookings")
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleDTO> fetchProviderBookings() throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return productService.fetchProviderBookings( authentication.getName());
    }

    @GetMapping("/client/bookings")
    @ResponseStatus(HttpStatus.OK)
    public List<ScheduleDTO> fetchClientBookings() throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return productService.fetchClientBookings( authentication.getName());
    }

    @PatchMapping("/provider/details")
    @ResponseStatus(HttpStatus.OK)
    public ServiceProviderDTO updateProviderDetails(@RequestBody ServiceProviderDTO provider) throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        provider.setUserId(authentication.getName());
        return productService.updateProviderDetails(provider);
    }

    @PostMapping("/provider/details")
    @ResponseStatus(HttpStatus.OK)
    public ServiceProviderDTO createProviderDetails(@RequestBody ServiceProviderDTO provider) throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        provider.setUserId(authentication.getName());
        return productService.createProviderDetails(provider);
    }

    @PatchMapping("/service/schedule")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleDTO updateServiceSchedule(@RequestBody ScheduleDTO schedule) throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        schedule.setServiceProviderId(authentication.getName());
        return productService.updateServiceSchedule(schedule, authentication.getName());
    }

    @PostMapping("/service/schedule")
    @ResponseStatus(HttpStatus.OK)
    public ScheduleDTO createServiceSchedule(@RequestBody ScheduleDTO schedule) throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        schedule.setServiceProviderId(authentication.getName());
        return productService.createServiceSchedule(schedule, authentication.getName());
    }

    @PostMapping("/bookSchedule")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ScheduleDTO allocateSchedule(@RequestBody String scheduleIDString) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        ObjectMapper objectMapper = new ObjectMapper();
        ScheduleID scheduleID = objectMapper.readValue(scheduleIDString, ScheduleID.class);
//        String ScheduleDTO = partialPODTO;
        return productService.allocateSchedule(scheduleID, authentication.getName());
    }




}
