package com.example.groomupapi.products.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class FacebookProvider {

    @Id
    @Column(unique=true)
    String id;
    String name;
    String photoUrl;
    String sourceType;
    String token;

    public void setToken(String token){
        this.token = token;
    }

//    @OneToMany
//    List<CategoryServiceProvider> categoryServiceProvider;

//    @ManyToMany
//    List<Schedule> schedules;


//    public static Category of(String username, String color, String url) {
//        Category ctg = new Category();
//        ctg.title = username;
//        ctg.color = color;
//        ctg.url = url;
//
//        return ctg;
//    }
}
