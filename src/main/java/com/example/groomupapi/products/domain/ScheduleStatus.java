package com.example.groomupapi.products.domain;

public enum ScheduleStatus {
    OPEN, BOOKED, REJECTED, ACCEPTED, CLOSED

}
