package com.example.groomupapi.products.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)

@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"service_provider_id", "category_id"})
})
public class CategoryServiceProvider {

    @Id
    @GeneratedValue
    Long id;

    @Column(name = "service_provider_id")
    Long serviceProviderId;

    @Column(name = "category_id")
    Long categoryId;


    public static CategoryServiceProvider of(Long categoryId, Long serviceProviderId) {
        CategoryServiceProvider ctg = new CategoryServiceProvider();
        ctg.serviceProviderId = serviceProviderId;
        ctg.categoryId = categoryId;

        return ctg;
    }
}
