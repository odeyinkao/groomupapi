package com.example.groomupapi.products.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"user_id", "provider_id"})
})
public class FavoriteProvider {

    @Id
    @GeneratedValue
    Long id;
    String user_id;
    Long provider_id;

}
