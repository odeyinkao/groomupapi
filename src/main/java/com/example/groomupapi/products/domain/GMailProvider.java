package com.example.groomupapi.products.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
@Table(name = "gmail_provider")
public class GMailProvider {

    @Id
    @Column(unique=true)
    String id;
    String name;
    String familyName;
    String givenName;
    String photoUrl;
    String sourceType;
    String token;
    String email;
    String refreshToken;
    String accessToken;

    public void setToken(String value){
        this.token = value;
    }

}
