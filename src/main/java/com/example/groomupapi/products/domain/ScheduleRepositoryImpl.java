package com.example.groomupapi.products.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class ScheduleRepositoryImpl implements CustomScheduleRepository {
    @Autowired
    EntityManager em;

    public List<Schedule> findScheduleByProviderAndCategory(String serviceCategoryId, Long serviceProviderId) {
        return em.createQuery("select s from Schedule s where s.categoryId = ?1 and s.serviceProviderId = ?2 ",
                Schedule.class)
                .setParameter(1, serviceProviderId)
                .setParameter(2, serviceCategoryId)
                .getResultList();
    }

    public List<Schedule> findScheduleByOwner(String serviceProviderId){
        return em.createQuery("select s from Schedule s where s.serviceProviderId = ?1 ",
                Schedule.class)
                .setParameter(1, serviceProviderId)
                .getResultList();
    }

    public List<Schedule>  fetchProviderBookings(String serviceProviderId){
        return em.createQuery("select s from Schedule s where s.serviceProviderId = ?1 order by start",
                Schedule.class)
                .setParameter(1, serviceProviderId)
                .getResultList();

    }

    public List<Schedule>  fetchClientBookings(String clientId){
        return em.createQuery("select s from Schedule s where s.allocatedId = ?1 order by start",
                Schedule.class)
                .setParameter(1, clientId)
                .getResultList();
    }
}
