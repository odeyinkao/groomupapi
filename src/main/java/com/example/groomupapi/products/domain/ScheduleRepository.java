package com.example.groomupapi.products.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long>, CustomScheduleRepository {
    public List<Schedule> fetchProviderBookings(String serviceProviderId);
    public List<Schedule> fetchClientBookings(String clientId);
}
