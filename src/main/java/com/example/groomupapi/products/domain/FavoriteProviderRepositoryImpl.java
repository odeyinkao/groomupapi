package com.example.groomupapi.products.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class FavoriteProviderRepositoryImpl implements CustomFavoriteProviderRepository {
    @Autowired
    EntityManager em;

    public List<FavoriteProvider>  findFavoriteByUserId(String userId){

        return em.createQuery("select fav from FavoriteProvider fav where fav.user_id = ?1",
                FavoriteProvider.class)
                .setParameter(1, userId)
                .getResultList();
    }

    public Optional<FavoriteProvider> findFavoriteByUserAndProviderId(String userId, Long providerId){

        return em.createQuery("select fav from FavoriteProvider fav where fav.user_id = ?1 and fav.provider_id = ?2",
                FavoriteProvider.class)
                .setParameter(1, userId)
                .setParameter(2, providerId)
                .getResultStream()
                .findFirst();
    }
}
