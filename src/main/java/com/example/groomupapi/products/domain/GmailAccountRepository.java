package com.example.groomupapi.products.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GmailAccountRepository extends JpaRepository<GMailProvider, String> {

}
