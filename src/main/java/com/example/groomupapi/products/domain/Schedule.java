package com.example.groomupapi.products.domain;

import com.example.groomupapi.common.domain.BusinessPeriod;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class Schedule {

    @Id
    @GeneratedValue
    Long id;

    BusinessPeriod period;

    @Enumerated(EnumType.STRING)
    ScheduleStatus status;

//    @OneToOne
//    Category category;

    @Column(name = "category_id")
    Long categoryId;

//    @ManyToOne
//    Customer allocated;

    String allocatedId;

    String serviceProviderId;

    @OneToOne
    Transaction transaction;

//    @ManyToOne
//    ServiceProvider serviceProvider;

    public static Schedule of(BusinessPeriod period, ScheduleStatus status, Long categoryId, String serviceProviderId) {
        Schedule schedule = new Schedule();
        schedule.period = period;
        schedule.status = status;

        schedule.categoryId = categoryId;
        schedule.serviceProviderId = serviceProviderId;


        return schedule;
    }


    public void setAllocatedId(String allocatedId){
        this.allocatedId =  allocatedId;
    }

    public void setStatus(ScheduleStatus status){
        this.status =  status;
    }

    public void setPeriod(BusinessPeriod period){
        this.period =  BusinessPeriod.of(period.getStart(), period.getEnd());
    }
    public void setCategoryId(Long categoryId){
        this.categoryId =  categoryId;
    }

    public void setServiceProviderId(String serviceProviderId){
        this.serviceProviderId =  serviceProviderId;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}