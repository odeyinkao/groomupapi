package com.example.groomupapi.products.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class Transaction {

    @Id
    @GeneratedValue
    Long id;
    Long scheduleId;
    double amount;
    LocalDateTime date;
    String comment;


}
