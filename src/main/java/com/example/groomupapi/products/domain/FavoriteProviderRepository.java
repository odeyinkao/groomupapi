package com.example.groomupapi.products.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FavoriteProviderRepository extends JpaRepository<FavoriteProvider, Long>, CustomFavoriteProviderRepository {
    public  List<FavoriteProvider>  findFavoriteByUserId(String userId);
    public Optional<FavoriteProvider> findFavoriteByUserAndProviderId(String userId, Long providerId);
}
