package com.example.groomupapi.products.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryServiceRepository extends JpaRepository<CategoryServiceProvider, Long> {
    @Query("select csp from CategoryServiceProvider csp where csp.categoryId = ?1 and csp.serviceProviderId = ?2")
    Optional<CategoryServiceProvider> findCSPByServiceAndCategory(Long categoryId, Long serviceProviderId);

}