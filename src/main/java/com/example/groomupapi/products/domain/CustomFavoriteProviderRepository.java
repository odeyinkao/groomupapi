package com.example.groomupapi.products.domain;

import java.util.List;
import java.util.Optional;

public interface CustomFavoriteProviderRepository {
    public  List<FavoriteProvider> findFavoriteByUserId(String userId);
    public Optional<FavoriteProvider> findFavoriteByUserAndProviderId(String userId, Long providerId);
}