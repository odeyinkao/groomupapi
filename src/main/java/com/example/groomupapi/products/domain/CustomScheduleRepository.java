package com.example.groomupapi.products.domain;

import java.util.List;

public interface CustomScheduleRepository {
    List<Schedule> findScheduleByProviderAndCategory(String serviceProviderId, Long serviceCategoryId);
    List<Schedule> findScheduleByOwner(String serviceProviderId);

}